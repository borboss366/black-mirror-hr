var socket = null;
let userid = (+ new Date()).toString();
let mac = ["ene", "bene", "raba"]

function connect() {
    console.log("Begin connect");
    socket = new WebSocket("ws://" + window.location.host + "/blackchat");

    socket.onerror = function() {
        console.log("socket error");
    };

    socket.onopen = function() {
        write("Connected");
    };

    socket.onclose = function(evt) {
        var explanation = "";
        if (evt.reason && evt.reason.length > 0) {
            explanation = "reason: " + evt.reason;
        } else {
            explanation = "without a reason specified";
        }

        write("Disconnected with close code " + evt.code + " and " + explanation);
        setTimeout(connect, 5000);
    };

    socket.onmessage = function(event) {
        received(event.data.toString());
    };
}

function received(message) {
    write(message);
}

function write(message) {
    var line = document.createElement("p");
    line.className = "message";
    line.textContent = message;

    var messagesDiv = document.getElementById("messages");
    messagesDiv.appendChild(line);
    messagesDiv.scrollTop = line.offsetTop;
}

function splitPairs(arr) {
    let pairs = [];
    for (let i=0 ; i < arr.length ; i+=2) {
        if (arr[i+1] !== undefined) {
            pairs.push ([arr[i], arr[i+1]]);
        }
    }
    return pairs;
};

function takeSplitValues(str) {
    let spl = str.split(" ").map(e => { return e.trim() });
    if (spl.length > 1) {
        return splitPairs(spl.slice(1));
    } else return []
}

function processSignalStrengths(text) {
    let splv = takeSplitValues(text);

    let obj = new Object();
    obj["id"] = userid;
    obj["signalStrengths"] = new Array();

    for (let i = 0; i < splv.length; i++) {
        let ss = new Object();
        ss["macAddress"] = splv[i][0];
        ss["strength"] = parseInt(splv[i][1]);
        obj["signalStrengths"].push(ss);
    }

    return "/uss " + JSON.stringify(obj);
}

function processSignalStrengthsPre(text) {
    let splv = text.split(" ").map(e => { return e.trim() }).slice(1);

    let obj = new Object();
    obj["id"] = userid;
    obj["signalStrengths"] = new Array();

    for (let i = 0; i < splv.length && i < mac.length; i++) {
        let ss = new Object();
        ss["macAddress"] = mac[i];
        ss["strength"] = parseInt(splv[i]);
        obj["signalStrengths"].push(ss);
    }

    return "/uss " + JSON.stringify(obj);
}

function processWho() {
    let obj = new Object()
    obj["id"] = userid;
    return "/who " + JSON.stringify(obj)
}

function processUserData(text) {
    let usdr = text.split(" ").map(e => { return e.trim() }).slice(1);

    let obj = new Object()
    obj["id"] = userid
    obj["name"] = usdr[0];
    return "/ud " + JSON.stringify(obj);
}

function processReputationData(text) {
    let usdr = text.split(" ").map(e => { return e.trim() }).slice(1);

    let obj = new Object()
    obj["userTo"] = userid
    obj["value"] = parseInt(usdr[1]);
    return "/ud " + JSON.stringify(obj);
}


function processImage(imageText) {
    let obj = new Object()
    obj["id"] = userid
    obj["imageStr"] = imageText;
    return "/image " + JSON.stringify(obj)
}

function getEncodedJSON(text) {
    let tt = text.trim();

    if (tt.startsWith("/uss")) {
        return processSignalStrengths(tt);
    } else if (tt.startsWith("/us")) {
        return processSignalStrengthsPre(tt);
    } else if (tt.startsWith("/reset")) {
        // just send an empty signal list
        return processSignalStrengths("/uss")
    } else if (tt.startsWith("/who")) {
        return processWho();
    } else if (tt.startsWith("/ud")) {
        return processUserData(tt)
    } else if (tt.startsWith("/rep")) {
        return processReputationData(tt)
    }

    return "";
}

function onSend() {
    var input = document.getElementById("commandInput");
    if (input) {
        var text = input.value;
        if (text && socket) {
            socket.send(getEncodedJSON(text));
            input.value = "";
        }
    }
}

//function toDataURL(src, callback, outputFormat) {
//  var img = new Image();
//  img.crossOrigin = 'Anonymous';
//  img.onload = function() {
//    var canvas = document.createElement('CANVAS');
//    var ctx = canvas.getContext('2d');
//    var dataURL;
//    canvas.height = this.naturalHeight;
//    canvas.width = this.naturalWidth;
//    ctx.drawImage(this, 0, 0);
//    dataURL = canvas.toDataURL(outputFormat);
//    callback(dataURL);
//  };
//  img.src = src;
//  if (img.complete || img.complete === undefined) {
//    img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
//    img.src = src;
//  }
//}
//
//function onImage() {
//    var input = document.getElementById("imageInput");
//    if (input) {
//        var text = input.value;
//
//        toDataURL(
//            text,
//            function(dataUrl) {
//                socket.send(processImage(dataUrl));
//            }
//        )
//    }
//}

function start() {
    connect();

    document.getElementById("usrid").innerText = "Your id is " + userid;
    document.getElementById("macs").innerText = "Macs are " + mac.join(" ");
    document.getElementById("sendButton").onclick = onSend;
    document.getElementById("commandInput").onkeydown = function(e) {
        if (e.keyCode == 13) {
            onSend();
        }
    };

//    document.getElementById("imageButton").onclick = onImage;
//    document.getElementById("imageInput").onkeydown = function(e) {
//        if (e.keyCode == 13) {
//            onImage();
//        }
//    };
}

function initLoop() {
    if (document.getElementById("sendButton")) {
        start();
    } else {
        setTimeout(initLoop, 300);
    }
}

initLoop();