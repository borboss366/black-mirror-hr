package cryqz.demo

import com.sxtanna.database.Kedis
import com.sxtanna.database.config.KedisConfig

fun main(args: Array<String>) {
    val kedis = Kedis(KedisConfig(
                    server = KedisConfig.ServerOptions("localhost"),
                    user = KedisConfig.UserOptions(auth = "cryqzrules")))
    kedis.enable()
    val resource = kedis.resource()
    resource.set("Tema", "is named Scytem")
    println(resource.get("Tema"))
}