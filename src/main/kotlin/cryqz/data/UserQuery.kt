package cryqz.data

data class UserQuery(val id: UserID) {
    override fun toString() = "{ \"id\": \"$id\" }"
    fun toViewString() = "Query $id"
}
