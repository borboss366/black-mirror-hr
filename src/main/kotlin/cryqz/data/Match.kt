package cryqz.data

data class Match(val id1: UserID, val id2: UserID) {
    fun toViewString() = "$id1 $id2"
}