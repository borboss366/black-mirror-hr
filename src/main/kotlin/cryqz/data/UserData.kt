package cryqz.data

data class UserData(
        val id: UserID,
        val name: String) {
    override fun toString() = "{ \"id\": \"$id\", \"name\": \"$name\" }"

    fun toViewString() = "$id - $name"
}
