package cryqz.data

import cryqz.utils.decodeToImage
import cryqz.utils.resizeToNorm

data class ImageData(
        val id: UserID,
        val imageStr: String) {
    fun makeImage() = decodeToImage(imageStr).map { resizeToNorm(it) }
    override fun toString() = "{ \"id\": \"$id\", \"image\": \"$imageStr\" }"
}
