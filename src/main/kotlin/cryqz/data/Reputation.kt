package cryqz.data

data class Reputation(
        val userTo: UserID,
        val value: Int)