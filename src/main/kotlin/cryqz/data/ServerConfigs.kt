package cryqz.data

data class ServerConfigs(
        val number2Match: Int,
        val maxDistance: Int)