package cryqz.data

data class SignalStrength(val macAddress: String, val strength: Int) {
    override fun toString() = "{ \"macAddress\" : \"$macAddress\", \"strength\" : $strength}"
}