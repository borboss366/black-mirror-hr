package cryqz.data

data class UserSignalData(
        val id: UserID,
        val signalStrengths: List<SignalStrength>) {
    override fun toString(): String {
        return "{ \"id\": \"$id\", \"signalStrengths\": [" +
                signalStrengths.map { it.toString() }.joinToString(",") +
                "]}"
    }

    fun toViewString(): String {
        return "$id ${signalStrengths.size}"
    }
}