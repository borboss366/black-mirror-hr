package cryqz.server.value

import cryqz.data.*
import cryqz.utils.resizeToNorm
import io.ktor.websocket.Frame
import io.ktor.websocket.WebSocketSession
import io.netty.util.internal.ConcurrentSet
import java.awt.image.BufferedImage
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

class BlackMirrorServer {
    // the amount of users, that are using black mirror
    private val usersCounter = AtomicInteger()

    // the data of the users that are using this software
    private val userData = ConcurrentHashMap<UserID, UserData>()

    // the sessions of the users
    private val userSession = ConcurrentHashMap<UserID, WebSocketSession>()

    // all of the images
    private val userImages = ConcurrentHashMap<UserID, BufferedImage>()
    private val userReputations = ConcurrentHashMap<UserID, MutableList<Int>>()
    private val userReputation = ConcurrentHashMap<UserID, Double>()

    private val matches = ConcurrentSet<Pair<UserID, UserID>>()

    private fun updateSession(userId: UserID, socket: WebSocketSession) {
        userSession[userId] = socket
    }

    private fun updateUserData(userId: UserID) {
        userData.computeIfAbsent(userId) {
            UserData(userId, "${usersCounter.incrementAndGet()}")
        }
    }

    private fun updateUserData(data: UserData) {
        userData.computeIfAbsent(data.id) {
            usersCounter.incrementAndGet()
            data
        }
    }

    private fun findMatches(userId: UserID): List<Match> {
        return userData.keys.filter { userId != it }.map{ Match(userId, it) }.toList()
    }

    suspend fun signalDataArrived(data: UserSignalData, socket: WebSocketSession) {
        updateSession(data.id, socket)
        updateUserData(data.id)

        message("/ok signal updated ${data.toViewString()}", socket)

        if (data.signalStrengths.isNotEmpty()) {
            findMatches(data.id).forEach {
                matches.add(Pair(it.id1, it.id2))
                matches.add(Pair(it.id2, it.id1))
                message("/match " + it.toViewString(), socket)
            }
        }
    }

    suspend fun userDataUpdated(data: UserData, socket: WebSocketSession) {
        updateSession(data.id, socket)
        updateUserData(data)
        message("/ok data updated ${data.toViewString()}", socket)
    }

    suspend fun userImageUpdated(userId: UserID, image: BufferedImage, socket: WebSocketSession) {
        val ava = resizeToNorm(image)
        userImages[userId] = ava
        updateUserData(userId)
        updateSession(userId, socket)
        message("/ok image updated (${image.width}, ${image.height})", socket)
    }

    suspend fun userReputationUpdated(rep: Reputation, socket: WebSocketSession) {
        val ur = userReputations.computeIfAbsent(rep.userTo) { mutableListOf(rep.value) }
        synchronized(ur) {
            ur.add(rep.value)
            val newRep = ur.average()
            userReputation[rep.userTo] = newRep
        }
        message("/rep ${String.format("%.2f", userReputation[rep.userTo]) }", socket)
        updateSession(rep.userTo, socket)
        updateUserData(rep.userTo)
    }


    suspend fun who(user: UserID, socket: WebSocketSession) {
        message(userData[user]?.toViewString() ?: "/error no such user!", socket)
    }

    suspend fun message(message: String, socket: WebSocketSession) {
        socket.send(Frame.Text(message))
    }
}