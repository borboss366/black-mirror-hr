package cryqz.server.value

import arrow.core.Either
import arrow.core.filterOrElse
import arrow.core.flatMap
import cryqz.data.*
import cryqz.utils.parseKlaxonEither
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.content.TextContent
import io.ktor.content.defaultResource
import io.ktor.content.resources
import io.ktor.content.static
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.withCharset
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.websocket.*
import kotlinx.coroutines.experimental.channels.consumeEach
import java.time.Duration

val bmServer = BlackMirrorServer()

fun Application.main() {
    install(DefaultHeaders)
    install(CallLogging)
    install(StatusPages) {
        status(HttpStatusCode.NotFound) {
            call.respond(TextContent("${it.value} ${it.description}", ContentType.Text.Plain.withCharset(Charsets.UTF_8), it))
        }
        exception<Throwable> { _ ->
            call.respond(HttpStatusCode.InternalServerError)
        }
    }

    install(WebSockets) {
        pingPeriod = Duration.ofMinutes(1)
    }

    install(Routing) {
        webSocket("/blackchat") {
            try {
                incoming.consumeEach { frame ->
                    if (frame is Frame.Text) {
                        receivedMessage(frame.readText(), this)
                    }
                }
            } finally {
            }
        }

        // this is the static resource for the web application
        static {
            defaultResource("index.html", "web")
            resources("web")
        }
    }
}


suspend fun receivedMessage(command: String, wss: WebSocketSession) {
    when {
        // update many signals
        command.startsWith("/uss")  -> {
            val p = parseKlaxonEither<UserSignalData>(command.substring(4))
            when (p) {
                is Either.Left ->  {
                    println(command.substring(4))
                    bmServer.message("/error parsing", wss)
                }
                is Either.Right -> bmServer.signalDataArrived(p.b, wss)
            }
        }

        // update user data
        command.startsWith("/ud")   ->  {
            val p = parseKlaxonEither<UserData>(command.substring(3))
            when(p) {
                is Either.Left -> {
                    println(command.substring(3))
                    bmServer.message("/error parsing", wss)
                }
                is Either.Right -> bmServer.userDataUpdated(p.b, wss)
            }
        }

        command.startsWith("/who")  -> {
            val p = parseKlaxonEither<UserQuery>(command.substring(4))
            when(p) {
                is Either.Left -> {
                    println(command.substring(4))
                    bmServer.message("/error parsing", wss)
                }
                is Either.Right -> bmServer.who(p.b.id, wss)
            }
        }

        command.startsWith("/image") -> {
            val p = parseKlaxonEither<ImageData>(command.substring(6))
                    .flatMap { id -> id.makeImage().map { Pair(id.id, it) } }
            when(p) {
                is Either.Left -> bmServer.message("/error parsing image", wss)
                is Either.Right -> bmServer.userImageUpdated(p.b.first, p.b.second, wss)
            }
        }

        command.startsWith("/rep") -> {
            val p = parseKlaxonEither<Reputation>(command.substring(4))
            when(p) {
                is Either.Left -> bmServer.message("/error parsing rep", wss)
                is Either.Right -> bmServer.userReputationUpdated(p.b, wss)
            }
        }


//        command.startsWith("/server") -> {
//            val p = parseKlaxonEither<Reputation>(command.substring(7))
//            when(p) {
//                is Either.Left -> bmServer.message("/error parsing server config", wss)
//                is Either.Right -> bmServer.serverConfigUpdated(p.b, wss)
//            }
//        }

        else -> bmServer.message("/error fuck you too", wss)
    }
}