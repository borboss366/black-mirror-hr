package cryqz.utils

import arrow.core.Either
import com.beust.klaxon.Klaxon
import io.ktor.http.HttpStatusCode
import java.io.File
import javax.imageio.ImageIO
import java.io.ByteArrayInputStream
import sun.misc.BASE64Decoder
import java.awt.Dimension
import java.awt.image.BufferedImage

data class HttpBinError(
        val request: String,
        val message: String,
        val code: HttpStatusCode,
        val cause: Throwable? = null
)

inline fun <reified T: Any> parseKlaxonEither(json: String): Either<String, T> {
    return try {
        val parseR = Klaxon().parse<T>(json)
        if (parseR != null) Either.Right(parseR)
        else Either.Left("Could not parse ${T::class.qualifiedName}")
    } catch(e: Exception) {
        Either.Left("Could not parse ${T::class.qualifiedName} ${e.message}")
    }
}


inline fun <reified T: Any> parseKlaxonFileEither(fileName: String): Either<String, T> {
    return try {
        val parseR = Klaxon().parse<T>(File(fileName))
        return if (parseR != null) Either.Right(parseR)
        else Either.Left("Could not parse ${T::class.qualifiedName}")
    } catch(e: Exception) {
        Either.Left("Could not parse ${T::class.qualifiedName} ${e.message}")
    }
}

fun decodeToImage(imageString: String): Either<String, BufferedImage> {
    return try {
        val decoder = BASE64Decoder()
        val imageByte = decoder.decodeBuffer(imageString)
        val bis = ByteArrayInputStream(imageByte)
        val image = ImageIO.read(bis)
        bis.close()
        if (image != null) Either.Right(image)
        else Either.Left("Could not parse image")
    } catch(e: Error) {
        Either.Left("Could not parse image ${e.message}")
    }
}

fun resizeToNorm(image: BufferedImage, dimension: Dimension = Dimension(400, 400)): BufferedImage {
    val res = BufferedImage(dimension.width, dimension.height, BufferedImage.TYPE_INT_RGB)
    val scaled = image.getScaledInstance(dimension.width, dimension.height, BufferedImage.SCALE_SMOOTH)
    res.graphics.drawImage(scaled, 0, 0, null)
    return res
}