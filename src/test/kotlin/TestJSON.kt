import arrow.core.contains
import arrow.core.flatMap
import arrow.syntax.foldable.forall
import cryqz.data.SignalStrength
import cryqz.data.UserData
import cryqz.data.UserSignalData
import cryqz.utils.parseKlaxonEither
import cryqz.utils.parseKlaxonFileEither
import io.kotlintest.specs.StringSpec

class TestJSON: StringSpec() {
    init {
        "test 239" {
            val ss = parseKlaxonFileEither<SignalStrength>("testfolder/json-tests/xxx-239.json")
            assert(ss.isRight())
            assert(ss.map { it.macAddress }.contains("xxx"))
            assert(ss.map { it.strength }.contains(239))
        }
        "test 366" {
            val ss = parseKlaxonFileEither<SignalStrength>("testfolder/json-tests/xxx-366.json")
            assert(ss.isRight())
            assert(ss.map { it.macAddress }.contains("xxx"))
            assert(ss.map { it.strength }.contains(366))
        }
        "test user data" {
            val ss = parseKlaxonFileEither<UserData>("testfolder/json-tests/ud-bobby-m.json")
            assert(ss.isRight())
            assert(ss.map { it.id }.contains("239"))
            assert(ss.map { it.name }.contains("bobby"))
        }
        "test bobby json" {
            val usd = parseKlaxonFileEither<UserSignalData>("testfolder/json-tests/xxx-239-bobby.json")
            assert(usd.isRight())
            assert(usd.map { it.id }.contains("bobby"))
            assert(usd.map { it.signalStrengths }.map { it.size }.contains(1) )
//            println(usd.map { it.toString() })
//            println(usd.map { it.toString() }.flatMap { parseKlaxonEither<UserSignalData>(it) })
//            assert(usd.map { it.toString() }.flatMap { parseKlaxonEither<UserSignalData>(it) }.contains(usd))
        }
    }
}