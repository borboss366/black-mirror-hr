FROM openjdk:8-jre-alpine

COPY ./build/libs/MatcherServer.jar /root/MatcherServer.jar

WORKDIR /root

CMD ["java", "-server", "-Xms300M", "-Xmx300M", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=100", "-XX:+UseStringDeduplication", "-jar", "MatcherServer.jar"]