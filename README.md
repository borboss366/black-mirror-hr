# Black mirror #

Have you seen the "Black Mirror" season 2 episode 1? Yeah, sure you did. This project is an attempt to implement it's
functionality. 

## Functionality ##
You wander around in an office or mall. The application will analyze the wifi and bluetooth signals and send information 
about it to the server. The server performs matching between nearby telephones. After that you can adjust the other person
reputation

## Message format ##

### From telephone to server ###
Telephone application sends the following json to the server

```javascript
{
  "id": {
    "id":"bobby"
  },
  "signalStrengths": [
    {
      "macAddress" : "xxx",
      "strength" : 239
    },
    {
      "macAddress" : "xxx2",
      "strength" : 240
    },
    {
      "macAddress" : "xxx3",
      "strength" : 366
    }
  ]
}
```

## Building ##
```
./gradlew build
sudo docker build -t borboss366/bm-metrics .
sudo docker push borboss366/bm-metrics
```


## Logic ##
One cannot set the reputation very often
One can set the reputation, only when the people are close


## TODO ##
1. Loading and getting of images
7. Images on web page
3. Loading and getting of parameters of algorithm
8. Reputation
9. So messages with repo is the one
https://www.gravatar.com/avatar/d50c83cc0c6523b4d3f6085295c953e0